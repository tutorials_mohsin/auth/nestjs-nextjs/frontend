import { axios } from "@/utils";

const forgetPassword = async (email: string) => {
  try {
    const responce = await axios.post("/auth/local/forgetpassword", { email });
    return responce.data;
  } catch (error: any) {
    console.log(error);
    if (error.response.status === 404) {
      return { "status": "error", "message": "User not found" }
    }
  }
};

export { forgetPassword };
