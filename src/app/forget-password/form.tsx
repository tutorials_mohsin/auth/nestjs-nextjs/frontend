"use client";

import Link from "next/link";
import React, { useEffect } from "react";

import { Form, Input } from "antd";
import { useAuth, useNotification } from "@/contexts";
import { useRouter } from "next/navigation";
import { forgetPassword } from "@/api";

const ForgetPasswordForm: React.FC = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { loading, isLoggedIn, checkIsLoggedIn } = useAuth();
  const { raiseNotification } = useNotification();

  useEffect(() => {
    checkIsLoggedIn();
    if (isLoggedIn) {
      router.push("/profile");
    }
  }, [isLoggedIn, router, checkIsLoggedIn]);

  const onFinish = async (values: any) => {
    console.log("Success:", values);
    const ret = await forgetPassword(values.email);
    if (ret?.status === "success") {
      raiseNotification("success", ret?.message);
    } else if (ret?.status === "error") {
      raiseNotification("error", ret?.message);
    } else {
      raiseNotification("error", "Something went wrong");
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-gray-100 dark:bg-gray-900 space-y-6 py-8 px-4 shadow sm:rounded-lg sm:px-10">
      <h1 className="text-3xl font-bold text-center text-gray-700 dark:text-gray-300">
        Logo
      </h1>

      <Form
        form={form}
        layout="vertical"
        name="login_form"
        initialValues={{ modifier: "public" }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="email"
          label={
            <span className="text-gray-800 dark:text-gray-200">Email</span>
          }
          rules={[{ required: true, message: "Please input your email" }]}
        >
          <Input
            type="email"
            autoComplete="email"
            className="block w-full px-4 py-2 mt-2 text-gray-700 dark:text-gray-300 bg-white dark:bg-gray-900 border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
          />
        </Form.Item>

        <Form.Item>
          <Input
            type="submit"
            disabled={loading}
            className="w-full px-4 py-2 tracking-wide bg-gray-400 dark:bg-gray-600 text-gray-800 dark:text-gray-200 hover:bg-gray-500 transition-colors duration-200 transform rounded-md focus:outline-none focus:bg-gray-600"
            value={"Sent verification code"}
          />
        </Form.Item>
      </Form>
      <div className="relative flex items-center justify-center w-full mt-6 border border-t">
        <div className="absolute px-5 bg-gray-100 text-black dark:bg-gray-900 dark:text-white">
          Or
        </div>
      </div>

      <p className="mt-4 text-sm text-center text-gray-700 dark:text-gray-300">
        {"Already have an account? "}
        <Link
          href="/login"
          className="font-medium text-blue-600 hover:underline"
        >
          Login
        </Link>
      </p>
    </div>
  );
};

export default ForgetPasswordForm;
