import React from "react";
import ForgetPasswordForm from "./form";

const LoginPage: React.FC = () => {
  return (
    <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden bg-gray-200 dark:bg-gray-800">
      <div className="w-full p-6 rounded-md shadow-lg lg:max-w-xl md:max-w-xl">
        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <ForgetPasswordForm />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
