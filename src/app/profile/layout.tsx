import React from "react";
import CheckAuth from "./checkAuth";

const ProfileLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <div>
        <CheckAuth />
      </div>
      <div>{children}</div>
    </div>
  );
};

export default ProfileLayout;
