"use client";
import { useAuth } from "@/contexts";
import { axios } from "@/utils";
import { useRouter } from "next/navigation";
import React, { useEffect } from "react";
const CheckAuth: React.FC = () => {
  const router = useRouter();
  const { login, logout, loading, isLoggedIn, checkIsLoggedIn } = useAuth();

  useEffect(() => {
    checkIsLoggedIn();
    console.log("isLoggedIn", isLoggedIn);
    if (!isLoggedIn) {
      router.push("/login");
    } else {
      router.push("/profile");
    }
  }, [isLoggedIn, router, checkIsLoggedIn]);

  const cookieCheck = async () => {
    const responce = await axios.get("");
  };

  return (
    <div>
      <p>Profile</p>
      <button onClick={logout}>Logout</button>
      <button onClick={cookieCheck}>Login</button>
    </div>
  );
};

export default CheckAuth;
