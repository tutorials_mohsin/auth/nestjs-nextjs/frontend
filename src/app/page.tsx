import Link from "next/link";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="bg-white text-black dark:bg-black dark:text-white">
        <Link href="/login">Login</Link>
      </div>
    </main>
  );
}
