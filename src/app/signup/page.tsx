import React from "react";
import SignupForm from "./form";
import GoogleOneTap from "@/components/auth/GoogleOneTap";

const LoginPage: React.FC = () => {
  return (
    <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden bg-gray-200 dark:bg-gray-800">
      <div className="w-full p-6 rounded-md shadow-lg lg:max-w-xl md:max-w-xl">
        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <SignupForm />
          <GoogleOneTap />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
