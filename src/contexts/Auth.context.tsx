"use client";
import React, { ReactNode, createContext, useContext, useState } from "react";

import { useRouter } from "next/navigation";
import { axios } from "@/utils";

interface AuthContextType {
  checkIsLoggedIn: () => Promise<void>;
  login: (email: string, password: string) => void;
  signup: (values: any) => void;
  logout: () => void;
  loading: boolean;
  isLoggedIn: boolean;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const router = useRouter();

  const checkIsLoggedIn = async () => {
    try {
      const responce = await axios.get("/auth/local/isloggedin");
      if (responce.data?.status === "success") {
        setIsLoggedIn(true);
      } else {
        setIsLoggedIn(false);
      }
    } catch (error: any) {
      console.log(error);
      setIsLoggedIn(false);
    }
  };

  const login = async (email: string, password: string) => {
    try {
      setLoading(true);
      const response = await axios.post("/auth/local/login", {
        email,
        password,
      });
      console.log("Response:", response.data);
      if (response.data?.status === "success") {
        setIsLoggedIn(true);
        setLoading(false);
        console.log("Logged in");
        router.replace("/profile");
      } else {
        setIsLoggedIn(false);
      }
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      setIsLoggedIn(false);
      console.log("Error:", error?.response?.data);
    }
  };

  const signup = async (values: any) => {
    try {
      setLoading(true);
      const response = await axios.post("/auth/local/register", values);
      console.log("Response:", response.data);
      if (response.data?.status === "success") {
        console.log("Signd up");
        router.replace("/profile");
      } else {
        setIsLoggedIn(false);
      }
    } catch (error: any) {
      setLoading(false);
      setIsLoggedIn(false);
      console.log("Error:", error?.response?.data);
    }
  };

  const logout = async () => {
    setIsLoggedIn(false);
    try {
      const responce = await axios.get("/auth/local/logout");
    } catch (error: any) {
      console.log(error);
    }
    router.replace("/login");
  };

  return (
    <AuthContext.Provider
      value={{ checkIsLoggedIn, login, signup, logout, loading, isLoggedIn }}
    >
      {children}
    </AuthContext.Provider>
  );
};
