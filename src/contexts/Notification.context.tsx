"use client";
import React, { ReactNode, createContext, useContext } from "react";

import { notification } from "antd";

type NotificationType = "success" | "info" | "warning" | "error";

interface NotificationContextType {
  raiseNotification: (type: NotificationType, message: string) => void;
}

const NotificationContext = createContext<NotificationContextType | undefined>(
  undefined
);

export const useNotification = () => {
  const context = useContext(NotificationContext);
  if (!context) {
    throw new Error(
      "useNotification must be used within an NotificationProvider"
    );
  }
  return context;
};

interface NotificationProviderProps {
  children: ReactNode;
}

export const NotificationProvider: React.FC<NotificationProviderProps> = ({
  children,
}) => {
  const [notificationapi, notificationHolder] = notification.useNotification();

  const raiseNotification = async (type: NotificationType, message: string) => {
    notificationapi[type]({
      message: message,
    });
  };

  return (
    <NotificationContext.Provider value={{ raiseNotification }}>
      {notificationHolder}
      {children}
    </NotificationContext.Provider>
  );
};
