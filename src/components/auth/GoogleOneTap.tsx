"use client";
import { useAuth } from "@/contexts";
import { axios } from "@/utils";
import { CredentialResponse, useGoogleOneTapLogin } from "@react-oauth/google";
import React from "react";
const GoogleOneTap: React.FC = () => {
  const { isLoggedIn, checkIsLoggedIn } = useAuth();

  const onSuccess = async (credentialResponse: CredentialResponse) => {
    const responce = await axios.post("/auth/local/googleonetap", {
      token: credentialResponse.credential,
    });
    checkIsLoggedIn();
  };

  const onError = () => {
    console.log("Error");
  };

  useGoogleOneTapLogin({
    onSuccess: onSuccess,
    onError: onError,
  });
  return <div></div>;
};

export default GoogleOneTap;
