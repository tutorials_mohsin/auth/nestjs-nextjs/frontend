/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    // "./src/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/feature/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily: {
      'Roboto': ['Roboto', 'sans-serif'],
      'Poppins': ['Poppins', 'sans-serif'],
      'KoHo': ['KoHo', 'sans-serif'],
    },
    boxShadow: {
      customShadow: [
        "10px 8px 8px 0 rgba(0, 0, 0, 0.2), 0 8px 17px 0 rgb(0 0 0 / 0.1)]",
      ],
    },
    extend: {
      animation: {
        blob: "blob 6s infinite",
      },
      keyframes: {
        blob: {
          "0%": {
            transform: "translate(0px, 0px) scale(1)",
          },
          "33%": {
            transform: "translate(30px, -50px) scale(1.1)",
          },
          "66%": {
            transform: "translate(-20px, 20px) scale(0.9)",
          },
          "100%": {
            transform: "tranlate(0px, 0px) scale(1)",
          },
        },
      },
    },
    variants: {
      extend: {
        lineClamp: ['responsive', 'hover', 'focus', 'group-hover'],
      },
    },
  },
  plugins: [
  ],
};